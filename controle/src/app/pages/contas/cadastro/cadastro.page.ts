import { ContaService } from '../service/conta-service';
import { NavController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {
  contasForm: FormGroup; 


  constructor(
    private builder: FormBuilder,
    private nav: NavController,
    private conta: ContaService
  ) { }

  ngOnInit() {
    this.initForm();

  }

  private initForm(){
    this.contasForm = this.builder.group({
      valor: ['',[Validators.required, Validators.min(0.01)]],
      parceiro: ['',[Validators.required, Validators.minLength(5)]],
      descricao: ['',[Validators.required, Validators.minLength(6)]],
      tipo: ['',[Validators.required]]
    });

  }

  /**
   * Salva a nova conta no Firebase.
   */
   registraConta(){
     const conta = this.contasForm.value;
     this.conta.registraConta(conta).then(()=> this.nav.navigateForward('contas/pagar'));
  }
}
