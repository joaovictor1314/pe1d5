// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBfWk_gttd9kR2EiA7aLCc6yYLWnszdhU4",
    authDomain: "controle-16ee3.firebaseapp.com",
    projectId: "controle-16ee3",
    storageBucket: "controle-16ee3.appspot.com",
    messagingSenderId: "291501754217",
    appId: "1:291501754217:web:0ae167a42e0495b322dff8",
    measurementId: "G-B55MKXXKQ2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
